const fullScreenRenderer = vtk.Rendering.Misc.vtkFullScreenRenderWindow.newInstance({background:[0,0,0,0]});
const vtkColorMaps = vtk.Rendering.Core.vtkColorTransferFunction.vtkColorMaps;

const sliceurls = ['https://joshmillar.gitlab.io/vtks/static/oakville/oakville_L3_ACH.vtp',
                   'https://joshmillar.gitlab.io/vtks/static/oakville/oakville_L3_AoA.vtp',
                   'https://joshmillar.gitlab.io/vtks/static/oakville/oakville_L3_U.vtp',
                ];

const stlurls = [{url: 'https://joshmillar.gitlab.io/vtks/static/oakville/walls.stl', opacity: "0.2"},
                ];

const streamurls = ['https://joshmillar.gitlab.io/vtks/static/oakville/oakville_Ustream.vtp',
                   ];

var renderer = fullScreenRenderer.getRenderer();
var renderWindow = fullScreenRenderer.getRenderWindow();
renderer.getActiveCamera().setPosition(-10.460718403281259, -41.079852143396536, 45.05497774499336);
renderer.getActiveCamera().setFocalPoint(-10.458100089004823, -41.076304136976084, 45.052607153719684);
renderer.getActiveCamera().setViewUp(0.03938795581347674, 0.5348629214142467, 0.8440202866241149);
renderer.setTwoSidedLighting(true);
renderer.setLightFollowCamera(true);

function displayStl(file) {
    stlurl = file.url;
    var stlMapper = vtk.Rendering.Core.vtkMapper.newInstance({ 
        scalarVisibility: false,
    });
    var stlActor = vtk.Rendering.Core.vtkActor.newInstance();
    var stlReader = vtk.IO.Geometry.vtkSTLReader.newInstance();

    stlActor.getProperty().setOpacity( file.opacity );
    stlActor.setMapper(stlMapper);
    stlMapper.setInputConnection(stlReader.getOutputPort());
    stlActor.getProperty().setColor(1, 1, 1);

    stlReader.setUrl(stlurl).then(() => {
        renderer.addActor(stlActor);
        renderer.resetCamera();
        renderWindow.render();
    });

}

for (var i = stlurls.length -1; i >= 0; i--)
{
    displayStl(stlurls[i]);
}

var lookupTable = vtk.Rendering.Core.vtkColorTransferFunction.newInstance();
var presetU = vtkColorMaps.getPresetByName('Rainbow Desaturated');

var vtkMapper = vtk.Rendering.Core.vtkMapper.newInstance({
        interpolateScalarsBeforeMapping: true,
        useLookupTableScalarRange: true,
        lookupTable,
        scalarVisibility: true,
    });

var vtkActor = vtk.Rendering.Core.vtkActor.newInstance();
var vtpReader = vtk.IO.XML.vtkXMLPolyDataReader.newInstance();

vtkMapper.setInputConnection(vtpReader.getOutputPort());
vtkActor.setMapper(vtkMapper);


vtkMapper.setInputConnection(vtpReader.getOutputPort());
// vtkActor.setMapper(vtkMapper);


fullScreenRenderer.addController('<table>\
    <tr>\
        <td>velPlane</td>\
            <td>\
                <input class="velPlane" type="checkbox" name="velPlane"/>\
            </td>\
        </tr>\
        <tr>\
            <td>velStream</td>\
                <td>\
                    <input class="velStream" type="checkbox" name="velStream"/>\
                </td>\
            </tr>\
        <tr>\
        </table>');

var velPlanecheckbox = document.querySelector("input[name=velPlane]")
var aoaPlanecheckbox = document.querySelector("input[name=AoAPlane]")
var velStreamcheckbox = document.querySelector("input[name=velStream]")
var achPlanecheckbox = document.querySelector("input[name=ACHPlane]")

velPlanecheckbox.addEventListener('change', function() {
 if(this.checked){
    vtpReader.setUrl(sliceurls[2]).then(() => {
        lookupTable.removeAllPoints();
        lookupTable.applyColorMap(presetU);
        lookupTable.setDiscretize(true);
        lookupTable.setNumberOfValues(16);
        vtkMapper.setScalarRange([0, 0.5]);
        vtkMapper.setColorByArrayName('magU');
        renderer.addActor(vtkActor);
        renderer.resetCamera();
        renderWindow.render();
    });
    } else {
        renderer.removeActor(vtkActor);
        renderWindow.render();
    }
});
velStreamcheckbox.addEventListener('change', function() {
 if(this.checked){
    vtpReader.setUrl(streamurls[0]).then(() => {
        lookupTable.removeAllPoints();
        lookupTable.applyColorMap(presetU);
        lookupTable.setDiscretize(true);
        lookupTable.setNumberOfValues(16);
        vtkMapper.setScalarRange([0, 0.5]);
        vtkMapper.setColorByArrayName('magU');
        renderer.addActor(vtkActor);
        renderer.resetCamera();
        renderWindow.render();
    });
    } else {
        renderer.removeActor(vtkActor);
        renderWindow.render();
    }
});
aoaPlanecheckbox.addEventListener('change', function() {
 if(this.checked){
    vtpReader.setUrl(sliceurls[1]).then(() => {
        lookupTable.removeAllPoints();
        lookupTable.applyColorMap(presetU);
        lookupTable.setDiscretize(true);
        lookupTable.setNumberOfValues(16);
        vtkMapper.setScalarRange([0, 1000]);
        vtkMapper.setColorByArrayName('AoA');
        renderer.addActor(vtkActor);
        renderer.resetCamera();
        renderWindow.render();
    });
    } else {
        renderer.removeActor(vtkActor);
        renderWindow.render();
    }
});
achPlanecheckbox.addEventListener('change', function() {
 if(this.checked){
    vtpReader.setUrl(sliceurls[0]).then(() => {
        lookupTable.removeAllPoints();
        lookupTable.applyColorMap(presetU);
        lookupTable.setDiscretize(true);
        lookupTable.setNumberOfValues(5);
        vtkMapper.setScalarRange([0, 10]);
        vtkMapper.setColorByArrayName('ACH');
        renderer.addActor(vtkActor);
        renderer.resetCamera();
        renderWindow.render();
    });
    } else {
        renderer.removeActor(vtkActor);
        renderWindow.render();
    }
});
